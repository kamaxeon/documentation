#!/bin/bash

set -euo pipefail

hugo server &
trap 'trap - EXIT && kill -- -$$' EXIT
sleep 3

# ignore
# - legacy favicons
# - GitLab URLs requiring login
# - GitLab links to markdown anchors that cannot be resolved as markdown is rendered dynamically
# - Red Hat internal websites
muffet http://localhost:1313/ \
    -e '.*favicon.ico$' \
    -e 'https://gitlab.com/cki-project/documentation/-/edit/.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/issues/new\?.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/forks/new' \
    -e 'https://gitlab.com/cki-project/documentation/-/commit/.*' \
    -e 'https://gitlab.com/cki-project/.*#.*' \
    -e 'https://gitlab.com/groups/cki-project/-/edit' \
    -e 'https://gitlab.com/projects/new\?namespace_id=3970123' \
    -e 'https://gitlab.com/redhat/red-hat-ci-tools/.*' \
    -e 'https://projects.engineering.redhat.com/.*' \
    -e 'https://gitlab.cee.redhat.com/.*' \
    -e 'https://documentation.internal.cki-project.org/.*' \
    -e 'https://source.redhat.com/.*' \
    -e 'https://brewweb.engineering.redhat.com/brew/.*' \
    -e 'https://applecrumble.internal.cki-project.org/.*' \
    -e 'https://datagrepper.engineering.redhat.com/.*' \
    -e 'https://gitlab.corp.redhat.com/.*' \
    -e 'https://datawarehouse.internal.cki-project.org'

# if no dangling links were found, shut down normally
kill %1
trap - EXIT
