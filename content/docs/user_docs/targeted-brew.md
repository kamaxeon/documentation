---
title: Targeted Testing for Brew/Koji Builds
linkTitle: KPET - RPM Testing
description: Framework which will execute subsystem related testing for RPM scratch builds
weight: 40
---

Targeted testing via scratch builds in brew/koji is available to developers who
wish to have their scratch builds tested, a brew/koji submitter can include
a string `test.cki.suffix` in the NVR name to trigger the testing. String names
can be be used to mirror the layout of kpet-db for each kernel subsystem, e.g.

- `test.cki`: Defaults to `test.cki.kt1`
- `test.cki.acpi`: "ACPI subsystem tests"
- `test.cki.bpf`: "BPF Kernel Self Tests"
- `test.cki.fs`: "Filesystem subsystem tests"
- `test.cki.ipmi`: "IPMI subsystem tests"
- `test.cki.kself`: "Kernel Self Tests"
- `test.cki.kt0`: KernelTier0 tests, smaller subset of kt1 tests used for
   gating brew builds
- `test.cki.kt1`: KernelTier1 tests, Tier1 tests across all kernel subsystems
- `test.cki.mem`: "Memory subsystem tests"
- `test.cki.net`: "Network subsystem tests"
- `test.cki.perf`: "Perf subsystem tests"
- `test.cki.rdma`: "RDMA subsystem tests"
- `test.cki.stor`: "Storage/RDMA subsystem tests"
- `test.cki.wdt`: "Watchdog driver tests (iTCO or wdat)"
- `test.cki.virt`: "Virtualization subsystem tests"

Please check with CKI team to request additional test sets, updated sets are identified
in [kpet-db sets] as listed in the main yaml.

To see which tests are generated, you can use the [kpet] CLI and use the `--sets`
option, e.g.

```bash
kpet run generate \
    --arch x86_64 \
    --kernel "##KPG_URL##" \
    --tree rhel8 \
    --sets kt1
```

See [KPET - Patch Testing](kpet.md) for more details.

Steps to enable this feature for your scratch builds:

1. submit a brew/koji task with the right NVR.

Some example NVRs:

- `kernel-4.18.0-129.el8.test.cki.bz1738631v1`
- `kernel-3.10.0-862.40.1.el7.test.cki.41.1.00`
- `kernel-4.18.0-128.el8.bz1728320.test.cki.net`
- `kernel-3.10.0-1067.el7.kpq2.test.cki.kt1`

The `.test.cki[.set]` part needs to follow the release. There are no other
requirements on the position and helper strings (e.g. Bugzilla numbers) can be
present both before and after it, like the above examples suggest.

**Note**: `test-cki` is supported by the CKI team as well, however actually
building a kernel with it results in `Illegal char '-' in: Release`. Feel free
to use this option if the build works for you or renaming is involved.

[kpet]: https://gitlab.com/cki-project/kpet
[kpet-db sets]: https://gitlab.com/cki-project/kpet-db/blob/main/index.yaml
